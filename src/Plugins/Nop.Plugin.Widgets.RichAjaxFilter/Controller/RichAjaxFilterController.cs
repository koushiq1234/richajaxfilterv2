﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Widgets.RichAjaxFilter.Factories;
using Nop.Plugin.Widgets.RichAjaxFilter.Models;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Controllers;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Controller
{
    public class RichAjaxFilterController : BasePublicController
    {
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly ICategoryService _categoryService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IRichAjaxFilterCatalogModelFactory _richAjaxFilterCatalogModelFactory;

        public RichAjaxFilterController(IAclService aclService,
                                        IStoreMappingService storeMappingService,
                                        IPermissionService permissionService,
                                        ICategoryService categoryService,
                                        IGenericAttributeService genericAttributeService,
                                        IWorkContext workContext,
                                        IWebHelper webHelper,
                                        IStoreContext storeContext,
                                        ICustomerActivityService customerActivityService,
                                        ILocalizationService localizationService,
                                        ICatalogModelFactory catalogModelFactory,
                                        IRichAjaxFilterCatalogModelFactory richAjaxFilterCatalogModelFactory)
        {
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _permissionService = permissionService;
            _categoryService = categoryService;
            _genericAttributeService = genericAttributeService;
            _workContext = workContext;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _customerActivityService = customerActivityService;
            _localizationService = localizationService;
            _catalogModelFactory = catalogModelFactory;
            _richAjaxFilterCatalogModelFactory = richAjaxFilterCatalogModelFactory;
        }
        public virtual async Task<IActionResult> Category(int categoryId, RichAjaxFilterCatalogProductsCommand command)
        {
            var category = await _categoryService.GetCategoryByIdAsync(categoryId);

            if (!await CheckCategoryAvailabilityAsync(category))
                return InvokeHttp404();

            //'Continue shopping' URL
            await _genericAttributeService.SaveAttributeAsync(await _workContext.GetCurrentCustomerAsync(),
                NopCustomerDefaults.LastContinueShoppingPageAttribute,
                _webHelper.GetThisPageUrl(false),
                (await _storeContext.GetCurrentStoreAsync()).Id);

            //display "edit" (manage) link
            if (await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel) && await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCategories))
                DisplayEditLink(Url.Action("Edit", "Category", new { id = category.Id, area = AreaNames.Admin }));

            //activity log
            await _customerActivityService.InsertActivityAsync("PublicStore.ViewCategory",
                string.Format(await _localizationService.GetResourceAsync("ActivityLog.PublicStore.ViewCategory"), category.Name), category);

            //model
            var model = await _richAjaxFilterCatalogModelFactory.PrepareCategoryModelAsync(category, command);

            model.CustomProperties["FilterData"] = new FilterData { CategoryId=categoryId, Command=command };
            //template
            var templateViewPath = await _catalogModelFactory.PrepareCategoryTemplateViewPathAsync(category.CategoryTemplateId);
            return View(templateViewPath, model);
        }
        public virtual async Task<IActionResult> GetCategoryProducts(int categoryId, RichAjaxFilterCatalogProductsCommand command)
        {
            var category = await _categoryService.GetCategoryByIdAsync(categoryId);

            if (!await CheckCategoryAvailabilityAsync(category))
                return NotFound();

            // var model = await _richAjaxFilterCatalogModelFactory.PrepareCategoryModelAsync(category, command);
            var model = await _richAjaxFilterCatalogModelFactory.PrepareRichAjaxFilterCatalogModelFactoryAsync(category, command);
                
            return PartialView("_ProductsInGridOrLines", model);
        }
        private async Task<bool> CheckCategoryAvailabilityAsync(Category category)
        {
            var isAvailable = true;

            if (category == null || category.Deleted)
                isAvailable = false;

            var notAvailable =
                //published?
                !category.Published ||
                //ACL (access control list) 
                !await _aclService.AuthorizeAsync(category) ||
                //Store mapping
                !await _storeMappingService.AuthorizeAsync(category);
            //Check whether the current user has a "Manage categories" permission (usually a store owner)
            //We should allows him (her) to use "Preview" functionality
            var hasAdminAccess = await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel) && await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCategories);
            if (notAvailable && !hasAdminAccess)
                isAvailable = false;

            return isAvailable;
        }
    }
}
