﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Widgets.RichAjaxFilter.Domain;
using Nop.Plugin.Widgets.RichAjaxFilter.Models;
using Nop.Plugin.Widgets.RichAjaxFilter.Models.Catalog;
using Nop.Services.Catalog;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Services
{
    public interface IRichAjaxFilterService
    {
        Task<IList<RichAjaxFilterSpecificationAttributeOptionFilter>> GetRichAjaxFilterFiltrableSpecificationAttributeOptionsByCategoryIdAsync(int categoryId, RichAjaxFilterCatalogProductsCommand command);
        List<Vendor> GetVendors(Category category);
        Task<IPagedList<Product>> SearchProductsModifiedAsync(
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            IList<int> categoryIds = null,
            IList<int> manufacturerIds = null,
            int storeId = 0,
            int vendorId = 0,
            IList<int> vendorIds = null,
            int warehouseId = 0,
            ProductType? productType = null,
            bool visibleIndividuallyOnly = false,
            bool excludeFeaturedProducts = false,
            decimal? priceMin = null,
            decimal? priceMax = null,
            int productTagId = 0,
            string keywords = null,
            bool searchDescriptions = false,
            bool searchManufacturerPartNumber = true,
            bool searchSku = true,
            bool searchProductTags = false,
            int languageId = 0,
            IList<SpecificationAttributeOption> filteredSpecOptions = null,
            IList<ProductAttributeValue> productAttributeOptions = null,
            ProductSortingEnum orderBy = ProductSortingEnum.Position,
            bool showHidden = false,
            bool? overridePublished = null);
        Task<IList<SpecificationAttributeOption>> GetFilterableOptions(IList<RichAjaxFilterSpecificationAttributeOptionFilter> models);

        Task<IList<RichAjaxFilterManufacturer>> GetRichAjaxFilterManufacturerFilterModel(int id);
    }
}
