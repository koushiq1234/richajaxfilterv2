﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Vendors;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Catalog;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.RichAjaxFilter.Models;
using Nop.Plugin.Widgets.RichAjaxFilter.Models.Catalog;
using Nop.Plugin.Widgets.RichAjaxFilter.Domain;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Services
{
    public class RichAjaxFilterService : IRichAjaxFilterService
    {
        private IRepository<ProductCategory> _productCategoryRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        private readonly IAclService _aclService;
        private readonly IRepository<ProductWarehouseInventory> _productWarehouseInventoryRepository;
        private readonly ILanguageService _languageService;
        private readonly IRepository<ProductAttributeCombination> _productAttributeCombinationRepository;
        private readonly IRepository<ProductManufacturer> _productManufactureRepository;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<SpecificationAttribute> _specificationAttributeRepository;
        private readonly IRepository<ProductSpecificationAttribute> _productSpecificationAttributeRepository;
        private readonly IRepository<SpecificationAttributeOption> _specificationAttributeOptionRepository;
        private readonly IRepository<Manufacturer> _manufacturerRepository;
        private readonly IRepository<ProductAttributeMapping> _productAttributeMappingRepository;
        private readonly IRepository<ProductAttributeValue> _productAttributeValueRepository;
        private readonly IRepository<ProductProductTagMapping> _productTagMappingRepository;
        private readonly IRepository<ProductTag> _productTagRepository;
        private readonly IRepository<LocalizedProperty> _localizedPropertyRepository;
        private readonly IRepository<ProductManufacturer> _productManufacturerRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly CatalogSettings _catalogSettings;
        private readonly IStoreContext _storeContext;
        private readonly ICategoryService _categoryService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IManufacturerService _manufacturerService;

        public RichAjaxFilterService(IRepository<ProductCategory> productCategoryRepository,
                                     IRepository<Product> productRepository,
                                     IRepository<Vendor> vendorRepository,
                                     IStoreMappingService storeMappingService,
                                     IWorkContext workContext,
                                     IAclService aclService,
                                     IRepository<ProductWarehouseInventory> productWarehouseInventoryRepository,
                                     ILanguageService languageService,
                                     IRepository<ProductAttributeCombination> productAttributeCombinationRepository,
                                     IRepository<ProductManufacturer> productManufactureRepository,
                                     IRepository<Category> categoryRepository,
                                     IRepository<SpecificationAttribute> specificationAttributeRepository,
                                     IRepository<ProductSpecificationAttribute> productSpecificationAttributeRepository,
                                     IRepository<SpecificationAttributeOption> specificationAttributeOptionRepository,
                                     IRepository<Manufacturer> manufacturerRepository,
                                     IRepository<ProductAttributeMapping> productAttributeMappingRepository,
                                     IRepository<ProductAttributeValue> productAttributeValueRepository,
                                     IRepository<ProductProductTagMapping> productTagMappingRepository,
                                     IRepository<ProductTag> productTagRepository,
                                     IRepository<LocalizedProperty> localizedPropertyRepository,
                                     IRepository<ProductManufacturer> productManufacturerRepository,
                                     IStaticCacheManager staticCacheManager,
                                     CatalogSettings catalogSettings,
                                     IStoreContext storeContext,
                                     ICategoryService categoryService,
                                     ISpecificationAttributeService specificationAttributeService,
                                     IManufacturerService manufacturerService
                                   )
        {
            _productCategoryRepository = productCategoryRepository;
            _productRepository = productRepository;
            _vendorRepository = vendorRepository;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
            _aclService = aclService;
            _productWarehouseInventoryRepository = productWarehouseInventoryRepository;
            _languageService = languageService;
            _productAttributeCombinationRepository = productAttributeCombinationRepository;
            _productManufactureRepository = productManufactureRepository;
            _categoryRepository = categoryRepository;
            _specificationAttributeRepository = specificationAttributeRepository;
            _productSpecificationAttributeRepository = productSpecificationAttributeRepository;
            _specificationAttributeOptionRepository = specificationAttributeOptionRepository;
            _manufacturerRepository = manufacturerRepository;
            _productAttributeMappingRepository = productAttributeMappingRepository;
            _productAttributeValueRepository = productAttributeValueRepository;
            _productTagMappingRepository = productTagMappingRepository;
            _productTagRepository = productTagRepository;
            _localizedPropertyRepository = localizedPropertyRepository;
            _productManufacturerRepository = productManufacturerRepository;
            _staticCacheManager = staticCacheManager;
            _catalogSettings = catalogSettings;
            _storeContext = storeContext;
            _categoryService = categoryService;
            _specificationAttributeService = specificationAttributeService;
            _manufacturerService = manufacturerService;
        }
        public List<Vendor>  GetVendors (Category category)
        {
            var queryVendor = from productCategory in _productCategoryRepository.Table
                              join product in _productRepository.Table on productCategory.ProductId equals product.Id
                              join vendor in _vendorRepository.Table on product.VendorId equals vendor.Id
                              where productCategory.CategoryId == category.Id && product.VendorId != 0
                              select vendor;

            return (queryVendor).Distinct().ToList();
        }

       
        public virtual async Task<IPagedList<Product>> SearchProductsModifiedAsync(
            int pageIndex = 0,
            int pageSize = int.MaxValue,
            IList<int> categoryIds = null,
            IList<int> manufacturerIds = null,
            int storeId = 0,
            int vendorId = 0,
            IList<int> vendorIds = null,
            int warehouseId = 0,
            ProductType? productType = null,
            bool visibleIndividuallyOnly = false,
            bool excludeFeaturedProducts = false,
            decimal? priceMin = null,
            decimal? priceMax = null,
            int productTagId = 0,
            string keywords = null,
            bool searchDescriptions = false,
            bool searchManufacturerPartNumber = true,
            bool searchSku = true,
            bool searchProductTags = false,
            int languageId = 0,
            IList<SpecificationAttributeOption> filteredSpecOptions = null,
            IList<ProductAttributeValue> productAttributeOptions = null,
            ProductSortingEnum orderBy = ProductSortingEnum.Position,
            bool showHidden = false,
            bool? overridePublished = null)
        {
            //some databases don't support int.MaxValue
            if (pageSize == int.MaxValue)
                pageSize = int.MaxValue - 1;

            var productsQuery = _productRepository.Table;

            if (!showHidden)
                productsQuery = productsQuery.Where(p => p.Published);
            else if (overridePublished.HasValue)
                productsQuery = productsQuery.Where(p => p.Published == overridePublished.Value);

            //apply store mapping constraints
            productsQuery = await _storeMappingService.ApplyStoreMapping(productsQuery, storeId);

            //apply ACL constraints
            if (!showHidden)
            {
                var customer = await _workContext.GetCurrentCustomerAsync();
                productsQuery = await _aclService.ApplyAcl(productsQuery, customer);
            }

            productsQuery =
                from p in productsQuery
                where !p.Deleted &&
                    (vendorId == 0 || p.VendorId == vendorId) &&
                    (
                        warehouseId == 0 ||
                        (
                            !p.UseMultipleWarehouses ? p.WarehouseId == warehouseId :
                                _productWarehouseInventoryRepository.Table.Any(pwi => pwi.Id == warehouseId && pwi.ProductId == p.Id)
                        )
                    ) &&
                    (productType == null || p.ProductTypeId == (int)productType) &&
                    (showHidden == false || LinqToDB.Sql.Between(DateTime.UtcNow, p.AvailableStartDateTimeUtc ?? DateTime.MinValue, p.AvailableEndDateTimeUtc ?? DateTime.MaxValue)) &&
                    (priceMin == null || p.Price >= priceMin) &&
                    (priceMax == null || p.Price <= priceMax)
                select p;

            if (!string.IsNullOrEmpty(keywords))
            {
                var langs = await _languageService.GetAllLanguagesAsync(showHidden: true);

                //Set a flag which will to points need to search in localized properties. If showHidden doesn't set to true should be at least two published languages.
                var searchLocalizedValue = languageId > 0 && langs.Count() >= 2 && (showHidden || langs.Count(l => l.Published) >= 2);

                IQueryable<int> productsByKeywords;

                productsByKeywords =
                        from p in _productRepository.Table
                        where p.Name.Contains(keywords) ||
                            (searchDescriptions &&
                                (p.ShortDescription.Contains(keywords) || p.FullDescription.Contains(keywords))) ||
                            (searchManufacturerPartNumber && p.ManufacturerPartNumber == keywords) ||
                            (searchSku && p.Sku == keywords)
                        select p.Id;

                //search by SKU for ProductAttributeCombination
                if (searchSku)
                {
                    productsByKeywords = productsByKeywords.Union(
                        from pac in _productAttributeCombinationRepository.Table
                        where pac.Sku == keywords
                        select pac.ProductId);
                }

                if (searchProductTags)
                {
                    productsByKeywords = productsByKeywords.Union(
                        from pptm in _productTagMappingRepository.Table
                        join pt in _productTagRepository.Table on pptm.ProductTagId equals pt.Id
                        where pt.Name == keywords
                        select pptm.ProductId
                    );

                    if (searchLocalizedValue)
                    {
                        productsByKeywords = productsByKeywords.Union(
                        from pptm in _productTagMappingRepository.Table
                        join lp in _localizedPropertyRepository.Table on pptm.ProductTagId equals lp.EntityId
                        where lp.LocaleKeyGroup == nameof(ProductTag) &&
                              lp.LocaleKey == nameof(ProductTag.Name) &&
                              lp.LocaleValue.Contains(keywords)
                        select lp.EntityId);
                    }
                }

                if (searchLocalizedValue)
                {
                    productsByKeywords = productsByKeywords.Union(
                                from lp in _localizedPropertyRepository.Table
                                let checkName = lp.LocaleKey == nameof(Product.Name) &&
                                                lp.LocaleValue.Contains(keywords)
                                let checkShortDesc = searchDescriptions &&
                                                lp.LocaleKey == nameof(Product.ShortDescription) &&
                                                lp.LocaleValue.Contains(keywords)
                                let checkProductTags = searchProductTags &&
                                                lp.LocaleKeyGroup == nameof(ProductTag) &&
                                                lp.LocaleKey == nameof(ProductTag.Name) &&
                                                lp.LocaleValue.Contains(keywords)
                                where
                                    (lp.LocaleKeyGroup == nameof(Product) && lp.LanguageId == languageId) && (checkName || checkShortDesc) ||
                                    checkProductTags

                                select lp.EntityId);
                }

                productsQuery =
                    from p in productsQuery
                    from pbk in LinqToDB.LinqExtensions.InnerJoin(productsByKeywords, pbk => pbk == p.Id)
                    select p;
            }

            if (categoryIds is not null)
            {
                if (categoryIds.Contains(0))
                    categoryIds.Remove(0);

                if (categoryIds.Any())
                {
                    var productCategoryQuery =
                        from pc in _productCategoryRepository.Table
                        where (!excludeFeaturedProducts || !pc.IsFeaturedProduct) &&
                            categoryIds.Contains(pc.CategoryId)
                        select pc;


                    /*
                         


                     */



                    productsQuery =
                        from p in productsQuery
                        where productCategoryQuery.Any(pc => pc.ProductId == p.Id)
                        select p;
                }
            }

            if (manufacturerIds is not null)
            {
                if (manufacturerIds.Contains(0))
                    manufacturerIds.Remove(0);

                if (manufacturerIds.Any())
                {
                    var productManufacturerQuery =
                        from pm in _productManufacturerRepository.Table
                        where (!excludeFeaturedProducts || !pm.IsFeaturedProduct) &&
                            manufacturerIds.Contains(pm.ManufacturerId)
                        select pm;

                    productsQuery =
                        from p in productsQuery
                        where productManufacturerQuery.Any(pm => pm.ProductId == p.Id)
                        select p;
                }
            }

            if (productTagId > 0)
            {
                productsQuery =
                    from p in productsQuery
                    join ptm in _productTagMappingRepository.Table on p.Id equals ptm.ProductId
                    where ptm.ProductTagId == productTagId
                    select p;
            }


            if (vendorIds?.Count > 0)
            {
                productsQuery =
                    from p in productsQuery
                    where vendorIds.Contains(p.VendorId)
                    select p;
            }

            if (filteredSpecOptions?.Count > 0)
            {
                var specificationAttributeIds = filteredSpecOptions
                    .Select(sao => sao.SpecificationAttributeId)
                    .Distinct();

                foreach (var specificationAttributeId in specificationAttributeIds)
                {
                    var optionIdsBySpecificationAttribute = filteredSpecOptions
                        .Where(o => o.SpecificationAttributeId == specificationAttributeId)
                        .Select(o => o.Id);

                    var productSpecificationQuery =
                        from psa in _productSpecificationAttributeRepository.Table
                        where psa.AllowFiltering && optionIdsBySpecificationAttribute.Contains(psa.SpecificationAttributeOptionId)
                        select psa;

                    productsQuery =
                        from p in productsQuery
                        where productSpecificationQuery.Any(pc => pc.ProductId == p.Id)
                        select p;
                }
            }

            if (productAttributeOptions?.Count > 0)
            {
                //var productAttributeValueIds = productAttributeOptions
                //                    .Select(sao=>sao.ProductAttributeMappingId)
                //var productAttributeMappingIds = new List<int>();
                //foreach (var item in collection)
                //{

                //}
                //foreach (var productAttributeValue in productAttributeOptions)
                //{
                //    productAttributeMappingIds.Add(productAttributeValue.ProductAttributeMappingId);

                //}

            }


            return await productsQuery.OrderBy(orderBy).ToPagedListAsync(pageIndex, pageSize);
        }

        public async Task<IList<RichAjaxFilterSpecificationAttributeOptionFilter>> GetRichAjaxFilterFiltrableSpecificationAttributeOptionsByCategoryIdAsync(int categoryId,RichAjaxFilterCatalogProductsCommand command)
        {
            if (categoryId <= 0)
                return new List<RichAjaxFilterSpecificationAttributeOptionFilter>();

            var productsQuery = await  GetAvailableProductsQueryAsync();

            IList<int> subCategoryIds = null;

            if (_catalogSettings.ShowProductsFromSubcategories)
            {
                var store = await _storeContext.GetCurrentStoreAsync();
                subCategoryIds = await _categoryService.GetChildCategoryIdsAsync(categoryId, store.Id);
            }

            var productCategoryQuery =
                from pc in _productCategoryRepository.Table
                where (pc.CategoryId == categoryId || (_catalogSettings.ShowProductsFromSubcategories && subCategoryIds.Contains(pc.CategoryId))) &&
                      (_catalogSettings.IncludeFeaturedProductsInNormalLists || !pc.IsFeaturedProduct)
                select pc;

            var query = from sao in _specificationAttributeOptionRepository.Table
                        join sar in _specificationAttributeRepository.Table on sao.SpecificationAttributeId equals sar.Id
                        join psm in _productSpecificationAttributeRepository.Table on sao.Id equals psm.SpecificationAttributeOptionId
                        join p in _productRepository.Table on psm.ProductId equals p.Id
                        join pcm in _productCategoryRepository.Table on p.Id equals pcm.ProductId
                        join c in _categoryRepository.Table on pcm.CategoryId equals c.Id

                        select new FilterCommands
                        {
                            P = p,
                            SAO = sao,
                            SA = sar,
                            PC = pcm,
                            C= c,
                            PSA=psm,
                           
                        };

            //var result = query;

            if(command.CategoryIds!=null && command.CategoryIds.Count>0)
            {
                query = from fc in query
                        where command.CategoryIds.Contains(fc.PC.CategoryId)
                         select fc;
            }

            if(command.SpecificationOptionIds != null && command.SpecificationOptionIds.Count>0)
            {
                query = from fc in query
                        where command.SpecificationOptionIds.Contains(fc.SAO.Id)
                         select fc;
            }

            if(command.VendorIds != null && command.VendorIds.Count>0)
            {
                query = from fc in query
                        where command.VendorIds.Contains(fc.V.Id)
                         select fc;
            }

            if(command.ProductAttributeValueIds != null && command.ProductAttributeValueIds.Count>0)
            {
                query = from fc in query
                        where command.ProductAttributeValueIds.Contains(fc.PAV.Id)
                         select fc;
            }

            var filterData = from fc in query
                             where fc.C.Id == categoryId && fc.PSA.AllowFiltering == true
                             
                             group fc.C by new
                             {
                                 fc.SAO.Id,
                                 fc.SAO.Name,
                                 fc.SAO.SpecificationAttributeId,
                                 fc.SAO.ColorSquaresRgb,
                                 fc.SAO.DisplayOrder
                             } into g
                             select new RichAjaxFilterSpecificationAttributeOptionFilter 
                             {   
                                 Id = g.Key.Id, 
                                 SpecificationAttributeId = g.Key.SpecificationAttributeId, 
                                 Name = g.Key.Name, ColorSquaresRgb = g.Key.ColorSquaresRgb, 
                                 Count = g.Count() 
                             };


            return await filterData.ToListAsync();

            //return result; 
            //var result =
            //    from sao in _specificationAttributeOptionRepository.Table
            //    join psa in _productSpecificationAttributeRepository.Table on sao.Id equals psa.SpecificationAttributeOptionId
            //    join p in productsQuery on psa.ProductId equals p.Id
            //    join pc in productCategoryQuery on p.Id equals pc.ProductId
            //    join sa in _specificationAttributeRepository.Table on sao.SpecificationAttributeId equals sa.Id
            //    where psa.AllowFiltering
            //    orderby
            //        sa.DisplayOrder, sa.Name,
            //        sao.DisplayOrder, sao.Name
            //    group p by new RichAjaxFilterSpecificationAttributeOptionFilterModel { Id=sao.Id,ColorSquaresRgb=sao.ColorSquaresRgb,Name=sao.Name,Selected=false/*,Count= productsQuery.Count()*/ }
            //    into gcs 
            //    select gcs.Key;

                         //    //linq2db don't specify 'sa' in 'SELECT' statement
                         //    //see also https://github.com/nopSolutions/nopCommerce/issues/5425
                         //    //select new { sa, sao };
                         //    //select new RichAjaxFilterSpecificationAttributeOptionFilterModel
                         //    //{
                         //    //    Id=  sao.Id,
                         //    //    ColorSquaresRgb = sao.ColorSquaresRgb,
                         //    //    Name = sao.Name,
                         //    //    Selected = false,
                         //    //    Count = productGroup.
                         //    //};
                         ////var cacheKey = _staticCacheManager.PrepareKeyForDefaultCache(
                         ////    NopCatalogDefaults.SpecificationAttributeOptionsByCategoryCacheKey, categoryId.ToString());
                         //var res = result.ToList();
                         //return res;

                         // var options =  await _staticCacheManager.GetAsync(cacheKey, async () => (await result.Distinct().ToListAsync()).Select(query => query.sao).ToList());
        }
        protected virtual async Task<IQueryable<Product>> GetAvailableProductsQueryAsync()
        {
            var productsQuery =
                from p in _productRepository.Table
                where !p.Deleted && p.Published &&
                      (p.ParentGroupedProductId == 0 || p.VisibleIndividually) &&
                      (!p.AvailableStartDateTimeUtc.HasValue || p.AvailableStartDateTimeUtc <= DateTime.UtcNow) &&
                      (!p.AvailableEndDateTimeUtc.HasValue || p.AvailableEndDateTimeUtc >= DateTime.UtcNow)
                select p;

            var store = await _storeContext.GetCurrentStoreAsync();
            var currentCustomer = await _workContext.GetCurrentCustomerAsync();

            //apply store mapping constraints
            productsQuery = await _storeMappingService.ApplyStoreMapping(productsQuery, store.Id);

            //apply ACL constraints
            productsQuery = await _aclService.ApplyAcl(productsQuery, currentCustomer);

            return productsQuery;
        }

        public async Task<IList<SpecificationAttributeOption>> GetFilterableOptions(IList<RichAjaxFilterSpecificationAttributeOptionFilter> models)
        {
            var list = new List<SpecificationAttributeOption>();
            foreach (var item in models)
            {
                list.Add(new SpecificationAttributeOption()
                {
                    Id=item.Id,
                    SpecificationAttributeId=item.SpecificationAttributeId,
                    ColorSquaresRgb = item.ColorSquaresRgb,
                    Name=item.Name
                });
            }
            return  await Task.FromResult(list);
        }

        public async Task<IList<RichAjaxFilterManufacturer>> GetRichAjaxFilterManufacturerFilterModel(int categoryId)
        {
            if (categoryId <= 0)
                return new List<RichAjaxFilterManufacturer>();

            // get available products in category
            var productsQuery =
                from p in _productRepository.Table
                where !p.Deleted && p.Published &&
                      (p.ParentGroupedProductId == 0 || p.VisibleIndividually) &&
                      (!p.AvailableStartDateTimeUtc.HasValue || p.AvailableStartDateTimeUtc <= DateTime.UtcNow) &&
                      (!p.AvailableEndDateTimeUtc.HasValue || p.AvailableEndDateTimeUtc >= DateTime.UtcNow)
                select p;

            var store = await _storeContext.GetCurrentStoreAsync();
            var currentCustomer = await _workContext.GetCurrentCustomerAsync();

            //apply store mapping constraints
            productsQuery = await _storeMappingService.ApplyStoreMapping(productsQuery, store.Id);

            //apply ACL constraints
            productsQuery = await _aclService.ApplyAcl(productsQuery, currentCustomer);

            var subCategoryIds = _catalogSettings.ShowProductsFromSubcategories
                ? await _categoryService.GetChildCategoryIdsAsync(categoryId, store.Id)
                : null;

            var productCategoryQuery =
                from pc in _productCategoryRepository.Table
                where (pc.CategoryId == categoryId || (_catalogSettings.ShowProductsFromSubcategories && subCategoryIds.Contains(pc.CategoryId))) &&
                      (_catalogSettings.IncludeFeaturedProductsInNormalLists || !pc.IsFeaturedProduct)
                select pc;

            // get manufacturers of the products
            var manufacturersQuery =
                from m in _manufacturerRepository.Table
                join pm in _productManufacturerRepository.Table on m.Id equals pm.ManufacturerId
                join p in productsQuery on pm.ProductId equals p.Id
                join pc in productCategoryQuery on p.Id equals pc.ProductId
                
                where !m.Deleted
                orderby
                   m.DisplayOrder, m.Name
                group m by new
                {
                    m.Id,
                    m.Name
                } into g 
                
                select new RichAjaxFilterManufacturer { 
                    Id=g.Key.Id,
                    Name=g.Key.Name,
                    Count=g.Count()
                };

            var key = _staticCacheManager
                .PrepareKeyForDefaultCache(NopCatalogDefaults.ManufacturersByCategoryCacheKey, categoryId.ToString());

            return  await _staticCacheManager.GetAsync(key, async () => await manufacturersQuery.Distinct().ToListAsync());
        }
    }
    
}
