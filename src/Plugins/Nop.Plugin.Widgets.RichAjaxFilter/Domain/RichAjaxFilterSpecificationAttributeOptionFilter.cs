﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Domain
{
    public partial record RichAjaxFilterSpecificationAttributeOptionFilter : BaseNopEntityModel, ILocalizedModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the specification attribute option name
        /// </summary>
        public string Name { get; set; }

        public int SpecificationAttributeId { get; set; }
        /// <summary>
        /// Gets or sets the specification attribute option color (RGB)
        /// </summary>
        public string ColorSquaresRgb { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the value is selected
        /// </summary>
        public bool Selected { get; set; }

        public int Count { get; set; }
        #endregion
    }
}
