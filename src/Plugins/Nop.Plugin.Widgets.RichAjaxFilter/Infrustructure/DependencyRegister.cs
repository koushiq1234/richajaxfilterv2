﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Widgets.RichAjaxFilter.Factories;
using Nop.Plugin.Widgets.RichAjaxFilter.Mvc.Routing;
using Nop.Plugin.Widgets.RichAjaxFilter.Services;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Infrustructure
{
    public class DependencyRegister : IDependencyRegistrar
    {
        public int Order => 222222;

        public void Register(IServiceCollection services, ITypeFinder typeFinder, AppSettings appSettings)
        {
            services.AddSingleton<IRichAjaxFilterRoutePublisher, RichAjaxFilterRoutePublisher>();
            services.AddScoped<RichAjaxFilterSlugRouteTransformer>();
            services.AddScoped<SlugRouteTransformer, RichAjaxFilterSlugRouteTransformer>();
            services.AddScoped<IRichAjaxFilterService, RichAjaxFilterService>();
            services.AddScoped<IRichAjaxFilterCatalogModelFactory, RichAjaxFilterCatalogModelFactory>();
        }
    }
}
