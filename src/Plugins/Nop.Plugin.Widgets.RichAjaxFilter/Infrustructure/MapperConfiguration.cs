﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Widgets.RichAjaxFilter.Models;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Infrustructure
{
    public class MapperConfiguration : Profile, IOrderedMapperProfile
    {
        public int Order => 11;

        public MapperConfiguration()
        {
            CreateMap<CatalogProductsModel, AjaxFilterCatalogProductsModel>()
                .ForMember(x => x.CategoryFilter, options => options.Ignore())
                .ForMember(x => x.VendorFilter, options => options.Ignore());
        }
    }
}
