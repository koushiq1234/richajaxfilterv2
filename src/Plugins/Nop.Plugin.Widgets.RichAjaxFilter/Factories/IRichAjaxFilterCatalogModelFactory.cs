﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Widgets.RichAjaxFilter.Domain;
using Nop.Plugin.Widgets.RichAjaxFilter.Models;
using Nop.Web.Factories;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Factories
{
    public interface IRichAjaxFilterCatalogModelFactory : ICatalogModelFactory
    {
        Task<AjaxFilterCatalogProductsModel> PrepareRichAjaxFilterCatalogModelFactoryAsync(Category category, RichAjaxFilterCatalogProductsCommand command);
        Task<RichAjaxFilterCategoryModel> PrepareCategoryModelAsync(Category category, RichAjaxFilterCatalogProductsCommand command);

        Task<RichAjaxFilterSpecificationFilterModel> PrepareRichAjaxFilterSpecificationFilterModel(IList<int> selectedOptions, IList<RichAjaxFilterSpecificationAttributeOptionFilter> availableOptions);
    }
}
