﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record RichAjaxFilterCategoryFilterModel : BaseNopModel
    {
        public RichAjaxFilterCategoryFilterModel()
        {
            Categories = new List<SelectListItem>();
        }
        public bool Enable { get; set; }
        public IList<SelectListItem> Categories { get; set; }
    }
}
