﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Widgets.RichAjaxFilter.Domain;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record RichAjaxFilterSpecificationAttributeFilterModel : BaseNopEntityModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the specification attribute name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the values
        /// </summary>
        public IList<RichAjaxFilterSpecificationAttributeOptionFilter> Values { get; set; }

       
        #endregion

        #region Ctor

        public RichAjaxFilterSpecificationAttributeFilterModel()
        {
            Values = new List<RichAjaxFilterSpecificationAttributeOptionFilter>();
        }

        #endregion
    }
}
