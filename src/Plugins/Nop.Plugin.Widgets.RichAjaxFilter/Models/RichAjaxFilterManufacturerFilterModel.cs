﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models.Catalog
{
    /// <summary>
    /// Represents a manufacturer filter model
    /// </summary>
    public partial record RichAjaxFilterManufacturerFilterModel : BaseNopModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether filtering is enabled
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the filtrable manufacturers
        /// </summary>
        public IList<SelectListItem> Manufacturers { get; set; }
        public  IList<int> Counts { get; set; }
        #endregion

        #region Ctor

        public RichAjaxFilterManufacturerFilterModel()
        {
            Manufacturers = new List<SelectListItem>();
            Counts = new List<int>();
        }

        #endregion
    }
}
