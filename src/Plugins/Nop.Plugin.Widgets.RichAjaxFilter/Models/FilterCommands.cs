﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public class FilterCommands
    {
        public SpecificationAttributeOption SAO { get; set; }
        public ProductAttributeValue PAV { get; set; }
        public Product P { get; set; }
        public ProductSpecificationAttribute PSA { get; set; }
        public ProductCategory PC { get; set; }
        public Vendor V { get; set; }
        public SpecificationAttribute SA { get; set; }
        public Category C { get; set; }
    }
}
