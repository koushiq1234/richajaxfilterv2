﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Models.Catalog;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public class FilterData
    {
        public RichAjaxFilterCatalogProductsCommand Command { get; set; }
        public  int CategoryId { get; set; }
    }
}
