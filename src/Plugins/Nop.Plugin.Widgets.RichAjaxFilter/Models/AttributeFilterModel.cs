﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record AttributeFilterModel : BaseNopModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether filtering is enabled
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the filtrable specification attributes
        /// </summary>
        //public IList<ProductAttributeFilterModel> Attributes { get; set; }

        #endregion

        #region Ctor

        public AttributeFilterModel()
        {
            //Attributes = new List<ProductAttributeFilterModel>();
        }

        #endregion
    }
}
