﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.UI.Paging;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record RichAjaxFilterCatalogProductsCommand : BasePageableModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the price ('min-max' format)
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Gets or sets the specification attribute option ids
        /// </summary>
        [FromQuery(Name = "specs")]
        public List<int> SpecificationOptionIds { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer ids
        /// </summary>
        [FromQuery(Name = "ms")]
        public List<int> ManufacturerIds { get; set; }

        /// <summary>
        /// Gets or sets the categories ids
        /// </summary>
        [FromQuery(Name = "ct")]
        public List<int> CategoryIds { get; set; }

        /// <summary>
        /// Gets or sets the vendors ids
        /// </summary>
        [FromQuery(Name = "vn")]
        public List<int> VendorIds { get; set; }
        /// <summary>
        /// Gets or sets the vendors ids
        /// </summary>
        [FromQuery(Name = "pav")]
        public List<int> ProductAttributeValueIds { get; set; }
        /// <summary>
        /// Gets or sets a order by
        /// </summary>
        public int? OrderBy { get; set; }

        /// <summary>
        /// Gets or sets a product sorting
        /// </summary>
        public string ViewMode { get; set; }

        #endregion
    }
}
