﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record VendorFilterModel : BaseNopModel
    {
        #region Ctor 
        public VendorFilterModel()
        {
            Vendors = new List<SelectListItem>();
        }
        #endregion 

        #region Properties 
        public bool Enable { get; set; }
        public IList<SelectListItem> Vendors { get; set; }
        #endregion
    }
}
