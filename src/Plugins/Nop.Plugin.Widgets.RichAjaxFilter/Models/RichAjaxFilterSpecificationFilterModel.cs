﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.RichAjaxFilter.Models
{
    public partial record RichAjaxFilterSpecificationFilterModel : BaseNopModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether filtering is enabled
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the filtrable specification attributes
        /// </summary>
        public IList<RichAjaxFilterSpecificationAttributeFilterModel> Attributes { get; set; }

        #endregion

        #region Ctor

        public RichAjaxFilterSpecificationFilterModel()
        {
            Attributes = new List<RichAjaxFilterSpecificationAttributeFilterModel>();
        }

        #endregion
    }
}
